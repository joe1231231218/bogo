﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GOBO_net
{
    public class BOGO
    {
        public static List<int> Monkey(string date, List<int> data)//BOGO演算法
        {
            ericrandom Rrandom = new ericrandom();
            List<int> ans = new List<int>();
            uint len = new int();
            uint nw_str = new int();
            nw_str = UInt32.Parse(date);
            Rrandom.srand(nw_str); //先推SEAD給RANDOM函式
            while (len > 0)
            {
                uint temp;//拿來接亂數
                len = (uint)data.Count;
                temp = Rrandom.rand() % len;
                ans.Add(data[(int)temp]);//答案的堆疊 PUSH
                data.RemoveAt((int)temp);//其他堆疊 POP
            }
            return ans;
        }
        public static List<int> Cutting(string data)//字串切成LIST
        {
            int len = data.Length;
            List<int> power = new List<int>();//切割後的LIST
            for (int i = 0; i < len; i++)
                power.Add((int)data[i]);
            return power;
        }

        public static int Main(string str,string date)
        {
            List<int> data = new List<int>(); //字串轉換之後的LIST
            List<int> ans = new List<int>();  //正確答案的LIST
            ans.Sort();
            data = Cutting(str);//先把得到的字串切割成LIST
            bool judge = true;//判斷是否全部都是相同的，全部都相同的話，JUDGE會變成FALSE，跳出迴圈
            int time= 0;//bogo做的次數
            while (judge)
            {
                data = Monkey(date, data);//實作BOGO演算法
                time++;//每做一次BOGO就+1 看到底做了幾次BOGO

                for (int i = 0; i < data.Count; i++)//檢查DATA跟ANS 每一個值是否相同
                {
                    if (data[i] != ans[i])//如果有錯的話，回傳true，讓while繼續檢查
                    {
                        judge = true;
                        break;
                    }
                    else//如果全部都是對的話，回傳false，並且跳出迴圈
                    {
                        judge = false;
                    }
                }
                if (time > 1576800000)//超過50年就跳出
                {
                    break;
                }
            }
            return time;
        }
    }
}
